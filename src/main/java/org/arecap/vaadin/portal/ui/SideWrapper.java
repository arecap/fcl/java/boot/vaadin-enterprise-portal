/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui;

import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public class SideWrapper extends FrameContainer {

    private final List<String> sectionIds = new LinkedList<>();

    private final Map<String, String> sectionIdsToCaptions = new LinkedHashMap<>();

    private final Map<String, String> sectionIdsToButtonStyles = new LinkedHashMap<>();

    private final Map<String, FrameContainer> sectionIdsToComponents = new LinkedHashMap<>();

    private final Map<String, FrameContainer> sectionIdsToHeaderComponents = new LinkedHashMap<>();

    private final FrameContainer sideBar = new FrameContainer();

    private final FrameContainer sideBarContent = new FrameContainer();

    public SideWrapper() {
        UI.getCurrent().addStyleName("sidebar-open");
        sideBar.setId("sidebar");
        sideBar.setStyleName(StyleConstantUtil.getComposedStyle(StyleConstantUtil.TRANSITIONED, "base"));
        sideBar.removeFrameworkStyle();
        addComponent(sideBar);
        sideBarContent.setId("sidebar-content");
        sideBarContent.removeFrameworkStyle();
        sideBar.addComponent(sideBarContent);
        removeFrameworkStyle();
    }

    public SideWrapper(String sectionId, String sectionCaption, String buttonStyle) {
        this();
        addSection(sectionId, sectionCaption, buttonStyle, true);
    }

    public void addSection(String sectionId, String sectionCaption, String buttonStyle) {
        setSectionReferences(sectionId, sectionCaption, buttonStyle);
        addContentHeader(sectionId, sectionCaption);
        addContent(sectionId);
    }

    public void addSection(String sectionId, String sectionCaption, String buttonStyle, boolean visible) {
        addSection(sectionId, sectionCaption, buttonStyle);
        if(visible) {
            setSectionVisible(sectionId);
        }
    }

    public void addComponent(String sectionId, Component component) {
        sectionIdsToComponents.get(sectionId).addComponent(component);
    }

    public List<String> getSectionIds() {
        return sectionIds;
    }

    public int sectionCount() {
        return sectionIds.size();
    }

    public String getButtonStyle(String sectionId) {
        return sectionIdsToButtonStyles.get(sectionId);
    }

    public Component getComponent(String sectionId) {
        return sectionIdsToComponents.get(sectionId);
    }

    public void setSectionVisible(String sectionId) {
        for(String configuredSectionId: sectionIds) {
            if(configuredSectionId.equalsIgnoreCase(sectionId)) {
                sectionIdsToHeaderComponents.get(configuredSectionId).setVisible(true);
                sectionIdsToComponents.get(configuredSectionId).addStyleName("opencontent");
                continue;
            }
            sectionIdsToHeaderComponents.get(configuredSectionId).setVisible(false);
            sectionIdsToComponents.get(configuredSectionId).removeStyleName("opencontent");
        }
    }

    public boolean isSectionVisible(String sectionId) {
        return sectionIdsToHeaderComponents.get(sectionId).isVisible();
    }

    private void setSectionReferences(String sectionId, String sectionCaption, String buttonStyle) {
        sectionIds.add(sectionId);
        sectionIdsToCaptions.put(sectionId, sectionCaption);
        sectionIdsToButtonStyles.put(sectionId, buttonStyle);
    }

    private void addContentHeader(String sectionId, String sectionCaption) {
        FrameContainer contentHeader = new FrameContainer();
        contentHeader.setStyleName("sidebar-header");
        contentHeader.removeFrameworkStyle();
        contentHeader.addComponent(new Label(sectionCaption));
        contentHeader.setVisible(false);
        sectionIdsToHeaderComponents.put(sectionId, contentHeader);
        sideBar.addComponent(contentHeader);
    }

    private void addContent(String sectionId) {
        FrameContainer content = new FrameContainer();
        content.setId(sectionId);
        content.setStyleName("sidebar-content-content");
        content.removeFrameworkStyle();
        sectionIdsToComponents.put(sectionId, content);
        sideBarContent.addComponent(content);
    }


}

