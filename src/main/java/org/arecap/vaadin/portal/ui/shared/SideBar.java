/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui.shared;

import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import org.arecap.vaadin.portal.ui.PortalSection;
import org.springframework.util.Assert;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public class SideBar extends CssLayout {


    public final static String SIDE_BAR_ID = "sidebar";

    public final static String SIDE_BAR_HEADER_MENU_ID = "sidebar-header-menu";

    public final static String SIDE_BAR_HEADER_INFO_ID = "sidebar-header-info";

    public final static String SIDE_BAR_HEADER_EDITOR_ID = "sidebar-header-editor";

    public final static String SIDE_BAR_CONTENT_ID = "sidebar-content";

    public final static String MENU_ID = "menu";

    public final static String SIDE_BAR_CONTENT = "sidebar-content-content";

    public final static String SIDE_BAR_HEADER = "sidebar-header";

    public SideBar() {
        super();
        CssLayout content = makeLayout(SIDE_BAR_CONTENT_ID);
        content.addComponent(makeLayout(MENU_ID, SIDE_BAR_CONTENT, PortalSection.STYLE_OPEN_CONTENT));
        this.addComponent(content);
        this.addComponent(makeLabel(SIDE_BAR_HEADER_MENU_ID, "Menu", SIDE_BAR_HEADER));
        this.addComponent(makeLabel(SIDE_BAR_HEADER_INFO_ID, "Info", SIDE_BAR_HEADER, PortalSection.STYLE_LEFT_FADE));
        this.addComponent(makeLabel(SIDE_BAR_HEADER_EDITOR_ID, "Editor", SIDE_BAR_HEADER, PortalSection.STYLE_LEFT_FADE));
    }

    protected SideBar(Component... children) {
        super(children);
    }

    protected CssLayout makeLayout(String id, String... styles) {
        CssLayout content = new CssLayout();
        content.setId(id);
        if(styles.length > 0) {
            content.setPrimaryStyleName(styles[0]);
            for(int i = 1; i < styles.length; i++) {
                content.setStyleName(styles[i], true);
            }
        }
        return content;
    }

    protected Label makeLabel(String id, String text, String... styles) {
        Label content = new Label();
        content.setId(id);
        content.setValue(text);
        Assert.isTrue(styles.length > 0);
        content.setPrimaryStyleName(styles[0]);
        for(int i = 1; i < styles.length; i++) {
            content.setStyleName(styles[i], true);
        }
        return content;
    }

}
