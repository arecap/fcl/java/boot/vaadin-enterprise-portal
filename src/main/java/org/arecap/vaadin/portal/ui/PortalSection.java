/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface PortalSection  {

    //Render indexes
    int CONTENT_RENDER_IND = 0;
    int OVERLAY_RENDER_IND = 100;


    //Section ids
    String BROWSER_ID = "browser";

    String BROWSER_CONTENT_ID = "browsercontent";

    String BREADCRUMBS_ID = "breadcrumbs";

    String SIDE_BAR_WRAPPER_ID = "sidebarwrapper";

    String SIDE_BAR_CONTROLLER_ID = "sidebarcontroller";

    //Generic styles
    String STYLE_TRANSITIONED = "transitioned";

    String STYLE_SELECTED = "transitioned";

    String STYLE_BASE = "base";

    String STYLE_LEFT_FADE = "leftfade";

    String STYLE_OPEN_CONTENT = "opencontent";

    String GWT_LABEL_STYLE = "gwt-Label";
}
