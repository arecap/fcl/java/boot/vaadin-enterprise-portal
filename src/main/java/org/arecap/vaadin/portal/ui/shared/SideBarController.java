/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui.shared;

import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.ui.CssLayout;
import org.arecap.vaadin.portal.ui.PortalSection;

import static org.arecap.vaadin.portal.ui.PortalSection.*;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface SideBarController {

    int RENDER_IND = PortalSection.OVERLAY_RENDER_IND + 3;

    String SIDE_BAR_BUTTON_MENU_ID = "sidebarbutton-menu-id";

    String SIDE_BAR_BUTTON_INFO_ID = "sidebarbutton-info-id";

    String SIDE_BAR_BUTTON_EDITOR_ID = "sidebarbutton-editor-id";

    String SIDE_BAR_CONTROLLER_CLASS = "sidebarbuttons";

    String SIDE_BAR_BUTTON = "sidebarbutton";

    String SIDE_BAR_BUTTON_MENU = "sidebarbutton-menu";

    String SIDE_BAR_BUTTON_INFO = "sidebarbutton-info";

    String SIDE_BAR_BUTTON_EDITOR = "sidebarbutton-editor";


    @GuiComponent(id = SIDE_BAR_CONTROLLER_ID, section = BROWSER_ID, renderOrder = RENDER_IND)
    default CssLayout getSideBarController() {
        CssLayout sideBarController = new CssLayout();
        sideBarController.setPrimaryStyleName(SIDE_BAR_CONTROLLER_CLASS);
        sideBarController.setStyleName(STYLE_TRANSITIONED, true);
        return sideBarController;
    }

    @GuiComponent(id = SIDE_BAR_BUTTON_EDITOR_ID, section = SIDE_BAR_CONTROLLER_ID, renderOrder = RENDER_IND + 1)
    default CssLayout getSideBarEditorButton() {
        CssLayout sideBarController = new CssLayout();
        sideBarController.setPrimaryStyleName(GWT_LABEL_STYLE);
        sideBarController.setStyleName(SIDE_BAR_BUTTON, true);
        sideBarController.setStyleName(SIDE_BAR_BUTTON_EDITOR, true);
        return sideBarController;
    }

    @GuiComponent(id = SIDE_BAR_BUTTON_INFO_ID, section = SIDE_BAR_CONTROLLER_ID, renderOrder = RENDER_IND + 2)
    default CssLayout getSideBarInfoButton() {
        CssLayout sideBarController = new CssLayout();
        sideBarController.setPrimaryStyleName(GWT_LABEL_STYLE);
        sideBarController.setStyleName(SIDE_BAR_BUTTON, true);
        sideBarController.setStyleName(SIDE_BAR_BUTTON_INFO, true);
        return sideBarController;
    }

    @GuiComponent(id = SIDE_BAR_BUTTON_MENU_ID, section = SIDE_BAR_CONTROLLER_ID, renderOrder = RENDER_IND + 3)
    default CssLayout getSideBarMenuButton() {
        CssLayout sideBarController = new CssLayout();
        sideBarController.setPrimaryStyleName(GWT_LABEL_STYLE);
        sideBarController.setStyleName(SIDE_BAR_BUTTON, true);
        sideBarController.setStyleName(SIDE_BAR_BUTTON_MENU, true);
        return sideBarController;
    }
}
