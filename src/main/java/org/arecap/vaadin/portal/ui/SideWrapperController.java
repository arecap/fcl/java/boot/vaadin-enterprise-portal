/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui;

import com.vaadin.event.MouseEvents;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.ui.UI;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public class SideWrapperController extends FrameContainer implements MouseEvents.ClickListener {


    private Map<String, FrameContainer> buttons = new LinkedHashMap<>();

    private final SideWrapper sideWrapper;

    public SideWrapperController(SideWrapper sideWrapper) {
        this.sideWrapper = sideWrapper;
        setStyleName(StyleConstantUtil.getComposedStyle("sidebarbuttons", StyleConstantUtil.TRANSITIONED));
        removeFrameworkStyle();
        configure();

    }

    public void configure() {
        removeAllComponents();
        buttons.clear();
        for(String sectionId: sideWrapper.getSectionIds()) {
            FrameContainer sideBarButton = new FrameContainer();
            sideBarButton
                    .setStyleName(StyleConstantUtil
                            .getComposedStyle("gwt-Label", "sidebarbutton",
                                    sideWrapper.getButtonStyle(sectionId)));
            if(sideWrapper.isSectionVisible(sectionId)) {
                sideBarButton.addStyleName(StyleConstantUtil.SELECTED);
            }
            sideBarButton.removeFrameworkStyle();
            sideBarButton.addClickListener(this);
            buttons.put(sectionId, sideBarButton);
            addComponent(sideBarButton, 0);
        }
    }


//    @Override
//    public void layoutClick(LayoutEvents.LayoutClickEvent event) {
//    }

    private void setSideWrapperOpen() {
        if(!UI.getCurrent().getStyleName().contains("sidebar-open")) {
            UI.getCurrent().addStyleName("sidebar-open");
        }
    }

    @Override
    public void click(MouseEvents.ClickEvent event) {
        if(event.getButton() == MouseEventDetails.MouseButton.LEFT) {
            if (event.getComponent().getStyleName().contains("sidebarbutton")) {
                FrameContainer pressedButton = (FrameContainer) event.getComponent();
                for (String sectionId : buttons.keySet()) {
                    if (pressedButton == buttons.get(sectionId)) {
                        if (pressedButton.getStyleName().contains(StyleConstantUtil.SELECTED)) {
                            UI.getCurrent().removeStyleName("sidebar-open");
                        } else {
                            pressedButton.addStyleName(StyleConstantUtil.SELECTED);
                            sideWrapper.setSectionVisible(sectionId);
                            setSideWrapperOpen();
                            continue;
                        }
                    }
                    if (buttons.get(sectionId).getStyleName().contains(StyleConstantUtil.SELECTED)) {
                        buttons.get(sectionId).removeStyleName(StyleConstantUtil.SELECTED);
                    }
                }
            }
        }
    }
}
