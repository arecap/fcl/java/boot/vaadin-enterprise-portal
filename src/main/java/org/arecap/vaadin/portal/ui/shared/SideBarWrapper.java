/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui.shared;

import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.ui.CssLayout;
import org.arecap.vaadin.portal.ui.PortalSection;

import static org.arecap.vaadin.portal.ui.PortalSection.*;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface SideBarWrapper {

    int RENDER_IND = PortalSection.OVERLAY_RENDER_IND + 2;

    @GuiComponent(id = SIDE_BAR_WRAPPER_ID, section = BROWSER_ID, renderOrder = RENDER_IND)
    default CssLayout getSideBarWrapper() {
        return new CssLayout();
    }

    @GuiComponent(id = SideBar.SIDE_BAR_ID, section = SIDE_BAR_WRAPPER_ID, renderOrder = RENDER_IND + 1)
    default CssLayout getSideBar() {
        CssLayout sideBarController = new SideBar();
        sideBarController.setPrimaryStyleName(STYLE_TRANSITIONED);
        sideBarController.setStyleName(STYLE_BASE, true);
        return sideBarController;
    }

}
