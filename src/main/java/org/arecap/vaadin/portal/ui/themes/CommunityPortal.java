/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui.themes;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface CommunityPortal {

//    int RENDER_IND = PortalSection.CONTENT_RENDER_IND + 1;
//
//    int IMPLEMENT_IND = 1000;
//
//    String BROWSER_PRIMARY_STYLE_NAME = "still";


//    @HtmlSection
//    default CssLayout getDisplay() {
//
////        CssLayout browserDisplay = new CssLayout();
////        browserDisplay.setSizeFull();
////        browserDisplay.setWidth("100%");
////        browserDisplay.setHeight("100%");
//        return new CssLayout();
//    }

//    @HtmlSection(id = "communitymainlayout", section = BROWSER_ID, renderOrder = 0)
//    default CssLayout getContent() {
//        CssLayout content = new CssLayout();
//        content.setPrimaryStyleName(STYLE_TRANSITIONED);
////        content.setStyleName();
//        return content;
//    }

//    @HtmlSection
//    default CustomLayout getDisplay() {
//
//        //        CssLayout browserDisplay = new CssLayout();
//        //        browserDisplay.setSizeFull();
//        //        browserDisplay.setWidth("100%");
//        //        browserDisplay.setHeight("100%");
//        return new CustomLayout("community");
//    }

}
