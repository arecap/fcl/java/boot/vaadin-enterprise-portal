/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.ui.shared;

import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.ui.CustomLayout;
import org.arecap.vaadin.portal.ui.PortalSection;

import static org.arecap.vaadin.portal.ui.PortalSection.BREADCRUMBS_ID;
import static org.arecap.vaadin.portal.ui.PortalSection.BROWSER_ID;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface Breadcrumbs {

    int RENDER_IND = PortalSection.OVERLAY_RENDER_IND + 1;

    @GuiComponent(id = BREADCRUMBS_ID, section = BROWSER_ID, renderOrder = RENDER_IND)
    default CustomLayout getBreadcrumbs() {
        return new CustomLayout("breadcrumb");
    }


}
